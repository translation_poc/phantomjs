# README #

A demo project of using PhantomJS for automation retrieval text resources

### What is this repository for? ###

Summary:
This is a POC project of using PhantomJS to walkthrough the WFO site to extract the text resources and compare the dataset of different languages vs pseudo language for hardcode/non-localize data.

Version: 0.1

### How do I get set up? ###

To run the code:

- Download PhantomJS: http://phantomjs.org/download.html

- Download jQuery 3.2.1: https://jquery.com/download/ and copy into the same folder with scripting files

- Extract and copy the scripting files into the same folder (or add class path into your environment variable)

- Trigger the script by running command: phantomjs [script_name] (please see --help for more option in case the script need more option from phantomjs)