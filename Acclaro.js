/***
To run this script with SSL protocol:
phantomjs --ignore-ssl-errors=yes --ssl-protocol=tlsv1 Acclaro.js
or can run script directly
phantomjs Acclaro.js
***/
var page = require('webpage').create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36';
page.viewportSize = { width: 1680, height: 1050 };

var loadInProgress = false;
var waitForPage = false;
var testIndex = 0;

page.onConsoleMessage = function(msg) { //overwrite this to write console in page context - page.evaluate()
	console.log(msg);
};

page.onLoadStarted = function() {
	loadInProgress = true;
	console.log('Load started!');
};

page.onLoadFinished = function(status) {
	loadInProgress = false;
	if (status != 'success') {
		console.log('Load failed: ' + status);
		phantom.exit();
	} else {
		console.log('Load finished!');
	}
};

page.onCallback = function(filename, data) { //overwrite this event to pass data from browser context - evaluate() - to phantom callback
	var fs = require('fs');
	fs.write(filename, data, 'a');
};

page.onError = function(msg, trace) { //overwrite to handle error message to write out or skip error output
    var msgStack = ['ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function(t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }
    //console.error(msgStack.join('\n')); //commend out this line to skip error output
};

// Define the steps to run on the automation
var steps = [
	function() { // Launch target wfo site
		console.log('=== Step: Open Login page');
		page.injectJs('jquery-3.2.1.min.js');
		page.open('https://support.qa.km.verintcloudservicesaws.com/km-ux/support/search');
	},
	
	function() { // Wait for Login page fully loaded
		waitForPage = page.evaluate(function() {
			return (document.getElementById('loginBaseLogo') == null);
		});
		if (waitForPage) {
			console.log("Wait for Login page ...");
		} else {
			console.log("Login page loaded!");
		}
	},
	
	function() { // Enter username and password then click LOGIN
		console.log('=== Step: Perform Login');
		page.evaluate(function() {
			document.getElementById('idToken1').value = 'acclaro';
			document.getElementById('idToken2').value = 'Translation1!';
			document.getElementById('loginButton_0').click();
			console.log('Login ...');
		});
	},
	
	function() { // Wait for Login page loading
		if (page.title.indexOf('Knowledge Management') < 0) {
			console.log('Wait KM page ...');
			waitForPage = true;
		} else {
			console.log('=== Step: KM loaded!');
			waitForPage = false;
		}
	},
	
	function() { // Ending stage
		console.log('Done!');
		page.render('KM.png', {format: 'png', quality: '50'});
		//require('fs').write('login.html', page.content, 'w');
	}
];

var mainInterval = setInterval(function() {
	if (!loadInProgress && typeof steps[testIndex] == 'function') {
		steps[testIndex]();
		if (!waitForPage) {
			testIndex++;
		}
	}
	if (typeof steps[testIndex] != 'function') {
		console.log('=== Exit! ===');
		clearInterval(mainInterval);
		phantom.exit();
	}
}, 1000);
