/***
Simple script to:
- Open WFO site
- Switch language
- Login WFO to Dashboard page - this need to change on waiting the page title
- Click the User Management navigation, click the Profile option to launch User Management Profile
- Traverse through all the children frames and extract the monitored class elements to retrieve the on screen texts (label, button, tooltip ...) then save into a text file

Trigger:
phantomjs --web-security=false UserProfileScript.js
***/
var page = require('webpage').create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36';
page.viewportSize = { width: 1680, height: 1050 };

var loadInProgress = false;
var waitForPage = false;
var testIndex = 0;

page.onConsoleMessage = function(msg) { //overwrite this to write console in page context - page.evaluate()
	console.log(msg);
};

page.onLoadStarted = function() {
	loadInProgress = true;
	console.log('Load started!');
};

page.onLoadFinished = function(status) {
	loadInProgress = false;
	if (status !== 'success') {
		console.log('Unable to access network');
		phantom.exit();
	} else {
		console.log('Load finished!');
	}
};

page.onCallback = function(filename, data) { //overwrite this event to pass data from browser context - evaluate() - to phantom callback
	//console.log('Write data into file: ' + filename);
	var fs = require('fs');
	fs.write(filename, data, 'a');
};

page.onError = function(msg, trace) { //overwrite to handle error message to write out or skip error output
    var msgStack = ['ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function(t) {
            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }
    //console.error(msgStack.join('\n')); //commend out this line to skip error output
};

// Define the steps to run on the automation
var steps = [
	function() { // Launch target wfo site
		console.log('=== Step: Open Login page');
		page.injectJs('jquery-3.2.1.min.js');
		//page.open('http://10.156.43.37/wfo/control/signin');
		page.open('http://10.156.14.59/wfo/control/signin');
	},
	
	function() { // Switch language - Make sure this site has languages enable
		console.log('=== Step: Switch language');
		page.evaluate(function() {
			var langOptions = document.getElementsByClassName('dropdown-content')[0];
			var newLang = langOptions.children[1].getAttribute('value');
			$('[name=language]')[0].value = newLang;
			handleLanguageChange($("[vname=Language]")[0]);
		});
	},
	
	function() { // Enter username and password then click GO
		console.log('=== Step: Perform Login');
		page.evaluate(function() {
			document.getElementById('username').value = 'wsuperuser';
			document.getElementById('password').value = 'pumpkin1';
			document.getElementById('loginToolbar_LOGINLabel').click();
			console.log('Open Dashboard ...');
		});
	},
	
	function() { // Wait for Dashboard page loading then click to appshell navigation
		if (page.title.indexOf('ⓓⓐⓢⓗⓑⓞⓐⓡⓓⓢ') < 0) {
			console.log('Wait Dasboards page ...');
			waitForPage = true;
		} else {
			console.log('=== Step: Dasboards loaded!');
			waitForPage = false;
			page.evaluate(function() {
				document.getElementById('appshell-nav-first-level-text').click();
			});
			
		}
	},
	
	function() { // Select User Management and User Profile
		console.log('=== Step: Open User Management Profile');
		page.evaluate(function() {
			$('[tabid="1_USER_MANAGEMENT"]').click();
			$('[tabid="1_USER_MANAGEMENT->2_BBM_EMPLOYEES->3_BBM_PEOPLE_PROFILE"]').click();
		});
	},
	
	function() { // Scan through all frames within User Profile and retrieve text element (* This is where we parse the page structure)
		console.log('=== Step: Traverse frames');
		page.evaluate(function() {
			function updateResult(inData, outData) {
				if (inData != null) {
					//inData = inData.replace(/\W/g, '');
					inData = inData.trim();
					if (inData != '') {
						outData += inData + '\n';
					}
				}
				return outData;
			}
			function extractData(bodyContent) {
				var result = '';
				var textClasses = ['x-btn-inner x-btn-inner-center', 
									'x-tip-body x-tip-body-default x-tip-body-default',
									'x-component nav-lvl1-text x-box-item x-component-default',
									'tableHeaderLightLeft',
									'headerText',
									'bpDropDownList',
									'x-btn create-filter-btn x-unselectable x-box-item x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon',
									'imgToolbarDefaultButton',
									'headerText1',
									'headerText2',
									'formRowDark25',
									'textLink',
									'dialogBtn',
									'tableHeaderImageButton',
									'buttonBlueDisabled',
									'buttonBlue',
									'smallgreenbutton',
									'buttonGreen'];
				var elementList, elementText;
				for (var i = 0; i < textClasses.length; i++) {
					var className = textClasses[i];
					result += '== Class: ' + className + '\n';
					elementList = bodyContent.getElementsByClassName(className);
					for (var j = 0; j < elementList.length; j++) {
						switch(className) {
							case 'bpDropDownList':
								var optionList = elementList[j].options;
								for (var k = 0; k < optionList.length; k++) {
									result = updateResult(optionList[k].text, result);
								}
								break;
							case 'x-btn create-filter-btn x-unselectable x-box-item x-btn-default-small x-icon x-btn-icon x-btn-default-small-icon':
								elementText = elementList[j].getAttribute('data-qtip');
								result = updateResult(elementText, result);
								break;
							case 'dialogBtn':
							case 'imgToolbarDefaultButton':
								elementText = elementList[j].getAttribute('fancytitle');
								result = updateResult(elementText, result);
							default:
								result = updateResult(elementList[j].innerText, result);
								break;
						}
					}
				}
				return result;
			};
			function traverseFrameHTML(frameId, frameBody) {
				//window.callPhantom(('HTML\\' + frameId + '.html'), frameBody.outerHTML);
				console.log('Scanning ' + frameId);
				var extractedData = '=== Frame: ' + frameId + '\n' + extractData(frameBody) + '\n';
				window.callPhantom(('userprofile.txt'), extractedData);
				var frameList = frameBody.getElementsByTagName('iframe');
				for (var i = 0; i < frameList.length; i++) {
					var fBody = frameList[i].contentWindow.document.body;
					traverseFrameHTML(frameList[i].id, fBody);
				}
			};
			traverseFrameHTML('userprofile', document.body);
			
		});
	},
	
	function() { // Ending stage
		console.log('Done!');
		page.render('userprofile.png', {format: 'png', quality: '50'});
		//fs.write('userprofile.html', page.content, 'w');
	}
];

var mainInterval = setInterval(function() {
	if (!loadInProgress && typeof steps[testIndex] == 'function') {
		//console.log("step: " + (testIndex + 1));
		steps[testIndex]();
		if (!waitForPage) {
			testIndex++;
		}
	}
	if (typeof steps[testIndex] != 'function') {
		console.log('=== Exit! ===');
		clearInterval(mainInterval);
		phantom.exit();
	}
}, 1000);
